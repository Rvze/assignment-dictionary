global exit
global string_length
global print_string
global print_string_error
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
section .text

; Вспомогательная функция для вывода в поток ошибок
error_stdout:
    mov rax, 1
    mov rdi, 2
    syscall
    ret

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60         ;'exit' syscall number
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax            ;rax - содержит длину строки. Если это не так, обнуление
.loop:
    cmp byte [rdi + rax], 0  ;Проверка на нуль-терминатор
    je .end                  ;выход из цикла
    inc rax                  ;иначе i++
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
   push rdi                 ;сохранение адреса строки
   call string_length       ;вызов функции
   pop rsi                  ;получаем адрес строки
   mov rdx, rax             ;сохраняем длину строки
   mov rax, 1               ;номер системного вызова
   mov rdi, 1               ;аргумент, записать в дескриптор
   syscall
   ret


; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_string_error:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    call error_stdout
    ret


; Принимает код символа и выводит его в stdout
print_char:
   push rdi                 ;записываем в вершину стека символ
   mov rdi, rsp             ;записываем адрес символа
   call print_string        ;вызываем print_string с адресом rdi = rsp
   pop rdi                  ;берем со стека
   ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi        ;в акк записываем число, для дальнейшей операции (rax%10)
    mov r9, 10          ;делитель
    mov rcx, rsp        ;помещаем в rcx адрес вершины стека
    dec rsp
    mov byte[rsp], 0
.loop:
    xor rdx, rdx
    div r9              ;делим rax на 10 и сохраняем остаток в rdx
    add rdx, '0'        ;переводим в ASCII
    dec rsp
    mov byte [rsp], dl   ;помещаем 8-битную цифру в буфер
    test rax, rax        ;проверяем, не закончилось ли число
jnz .loop

    mov rdi, rsp
    push rcx
    call print_string
    pop rcx
    mov rsp, rcx
    ret
.end:
    call print_string
    add rsp, 24
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi
    jns .print
    push rdi
    mov rdi, "-"
    call print_char
    pop rdi
    neg rdi
.print:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
.loop:
    mov r8b, byte[rsi]
    mov r9b, byte[rdi]
    cmp r8b, r9b
    jne .not_eq
    cmp r9b, 0
    je .eq
    inc rsi
    inc rdi
    jmp .loop
.eq:
    mov rax,1
    ret
.not_eq:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax,0       ;номер начала
    mov rdi,0       ;дескриптор
    mov rdx,1       ;длина символа
    push 0          ;резерв место для чтения
    mov rsi, rsp    ;копируем в rsi
    syscall
    pop rax         ;читаем
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор



read_word:
        xor rdx, rdx
        .main_loop:
                ; read_char с сохранением регистров
                push rdi
                push rsi
                push rdx
                call read_char
                pop rdx
                pop rsi
                pop rdi

                ; проверка на пробельные символы
                cmp rax, 0x20
                je .on_whitespace
                cmp rax, 0x9
                je .on_whitespace
                cmp rax, 0xA
                je .on_whitespace

                ; проверка на ошибку переполнения буффера
                cmp rdx, rsi
                je .on_error
                ; проверка на конец ввода
                test rax, rax
                je .end

                mov byte [rdi + rdx], al
                inc rdx
                jmp .main_loop

        .on_whitespace: ; обработка пробельных символов
                cmp rdx, 0 ; если символы в начале строки,
                je .main_loop ; переходим на следующую итерацию
                jmp .end ; в обратном случае, заканчиваем чтение

        .on_error:
                xor rax, rax
                ret

        .end:
                mov rax, rdi
                mov byte [rdi + rdx], 0
                ret




; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax        ;храним число в rax
    xor rcx, rcx        ;длина
    mov r9, 10          ;для умножений на 10
    xor rsi, rsi
    xor rdx, rdx

.loop:
    movzx r8, byte[rdi + rcx]     ;r9 = адрес цифры
    cmp r8b, '0'                ;
    jb .end                     ;если в r8 нет цифры, то завершаем прогармму
    cmp r8b, '9'
    ja .end
    xor rdx, rdx
    mul r9                      ;иначе умножаем на 10
    sub r8b, '0'                ;конвертируем строку в число
    add rax, r8
    inc rcx                     ;длина++
    jmp .loop
.end:
    mov rdx, rcx
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte[rdi] ,'-'
    je .sign
    call parse_uint
    jmp .end
.sign:
    inc rdi                 ;пропускаем знак
    call parse_uint
    cmp rdx, 0              ;если не удалось прочитать
    je .end
    neg rax                 ;-rax
    inc rdx                 ;длина строки + знак

.end:
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx            ;счетчик
    xor r9, r9
    call string_length
    push rax
    push rsi
.loop:
    cmp rcx, rdx            ;сравнение счетчика и кол-ва символов
    je .stop
    mov r9, [rdi+rcx]       ;кладем нынешний символ
    mov [rsi+rcx], r9       ;копируем
    cmp rax,0               ;сравнение с 0
    je .end
    dec rax                 ;--i
    inc rcx                 ;счетчик++
    jmp .loop
.stop:
    pop rsi
    pop rax
    mov rax,0
    ret
.end:
    pop rsi
    pop rax
    mov byte [rsi+rax],0
    ret