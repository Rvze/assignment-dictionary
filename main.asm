%define buffer_size 256
%define new_line 0xA

%include "colon.inc"
%include "lib.inc"
%include "words.inc"

extern find_word
global _start

section .rodata
ask_input: db "Enter the key", new_line, 0
not_found_key: db "There is no such key", new_line, 0
buffer_overflow: db "The key length is more than 256", new_line, 0

section .bss
word_buffer: resb buffer_size
section .text
_start:

        mov rdi, ask_input
        call print_string

        mov rdi, word_buffer
        mov rsi, buffer_size
        call read_word
        test rax, rax
        je .print_buffer_overflow

        push rdx
        mov rdi, word_buffer
        mov rsi, head
        call find_word
        test rax, rax
        je .print_key_not_found

        pop rdx
        add rax, 8
        add rax, rdx
        inc rax
        mov rdi, rax
        call print_string
        jmp .end

        .print_buffer_overflow:
                mov rsi, buffer_overflow
                call print_string_error
                jmp .end

        .print_key_not_found:
                mov rdi, not_found_key
                call print_string_error
                jmp .end

        .end:
                mov rax, 60
                mov rdi, 0
                syscall