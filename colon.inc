%define head 0

%macro colon 2
        %ifid %2
                %2:
                        dq head
                        %define head %2
        %else
                %fatal "wrong key"
        %endif

        %ifstr %1
                db %1, 0
        %else
                %fatal "wrong string"

        %endif
%endmacro